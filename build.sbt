name := "CombLogic"
enablePlugins(ScalaJSPlugin)

version := "0.1"
//libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.1.2"

scalaVersion := "2.13.1"

scalaJSUseMainModuleInitializer := true
libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "1.0.0"
libraryDependencies += "com.danielasfregola" %% "random-data-generator" % "2.9"