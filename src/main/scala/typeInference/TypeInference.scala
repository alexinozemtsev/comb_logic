package typeInference

import `type`.{Arrow, TyVar, Type}
import expr._

object Infer {

  def generateListNames =
    for {
      number <- 0 to 5
      letter <- 'a' to 'z'
    }
      yield
        if (number == 0)
          letter.toString
        else letter.toString + number.toString
  def VarSubstitution(s: String, typeNew: Type, typeToReplace: Type): Type =
    typeToReplace match {
      case TyVar(sToReplace) =>
        if (s == sToReplace) typeNew
        else TyVar(sToReplace)
      case Arrow(type1, type2) =>
        Arrow(VarSubstitution(s, typeNew, type1),
          VarSubstitution(s, typeNew, type2))
    }

  def unifyTypes(pair: (Type, Type), env: Map[Expr, Type]): Map[Expr, Type] = {
    pair match {
      case (TyVar(s), t) => env.view.mapValues(VarSubstitution(s, t, _: Type)).toMap
      case (t, TyVar(s)) => env.view.mapValues(VarSubstitution(s, t, _: Type)).toMap
      case (Arrow(t1, t2), Arrow(t3, t4)) =>
        val env1: Map[Expr, Type] = unifyTypes((t1, t3), env)
        unifyTypes((t2, t4), env1)
    }
  }

  def inferenceTypes(expr: Expr, names: IndexedSeq[String], env: Map[Expr, Type]): (Map[Expr, Type], IndexedSeq[String]) = {
    expr match {
      case C(I) => (env.updated(C(I), I.GetType(names)._1), I.GetType(names)._2)
      case C(K) => (env.updated(C(K), K.GetType(names)._1), K.GetType(names)._2)
      case C(S) => (env.updated(C(S), S.GetType(names)._1), S.GetType(names)._2)
      case App(expr1, expr2) =>
        val inferExpr2 = inferenceTypes(expr2, names, env)
        val Expr2Type = inferExpr2._1(expr2)
        val inferExpr1 = inferenceTypes(expr1, inferExpr2._2, inferExpr2._1)
        inferExpr1._1(expr1) match {
          case Arrow(t1, _) =>
            val env1 = unifyTypes((t1, Expr2Type), inferExpr1._1)
            val env2 = unifyTypes((t1, Expr2Type), env1)
            env2(expr1) match {
              case Arrow(_, t2) =>
                (env2.+(expr -> t2), inferExpr1._2)
            }
        }
    }
  }

  def inferenceTypes(expr: Expr):Type = {
    inferenceTypes(expr,generateListNames,Map())._1(expr)
  }
}