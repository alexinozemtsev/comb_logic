package gift
import expr.Expr
import typeInference.Infer.inferenceTypes
import expr.Expr.genExpr
import org.scalacheck.Gen

object QuestionGenerator {
  val exprList: List[Expr] = Gen.listOfN(4,genExpr(2,forLeft = false)).sample.get
  val rightAnswerNum: Int = Gen.choose(1,3).sample.get

  val rightAnswer: Expr = exprList(rightAnswerNum)
  val rightType = typeInference.Infer.inferenceTypes(rightAnswer)

  val answers = exprList.map(expr =>
    if (expr.toString == rightAnswer.toString)
      Answer(true, expr.toString)
    else
      Answer(false, expr.toString))

  val q = Question1(
    "Вывод типов",
    s"Какой комбинатор имеет тип ${rightType.toString}?",
    answers )
}
