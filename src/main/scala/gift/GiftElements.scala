package gift

case class Question1(title:String, text: String, answers: List[Answer]){
  override def toString: String =
    s"""::$title
       |::$text{
       |${answers.map(_.toString).mkString("\n")}
       |}""".stripMargin
}

case class Answer(isCorrect: Boolean, text: String){
  override def toString: String =
    (if (isCorrect) "=" else "~") + text
}



