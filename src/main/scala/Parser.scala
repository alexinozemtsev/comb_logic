import expr._
object Parser {

  private val head_comb = """\(?([KIS()]+)\)?([KIS])""".r
  private val head_brackets = """([KIS()]+)\(([KSI()]+)\)""".r
  private val comb_comb = """\(?([KIS])([KIS])\)?""".r
  private val comb = "([KIS])".r
  private val brackets = """/?\((.+)\)$""".r
  private val withMarker = """([KIS()]+)/([KIS()]+)""".r

  private def PutAppMarker(string: String): String = string match {
    case brackets(expr) => PutAppMarker(expr)
    case comb(_) => string
    case comb_comb(a, b) => a + "/" + b
    case head_comb(head, comb) => head + "/" + comb
    case head_brackets(head, brackets) =>
      string.substring(0, IndexOfApp(string)) +
        "/" + string.substring(IndexOfApp(string))
    case _ => "Incorrect input"
  }//App - Application

  private def IndexOfApp(string: String): Int = {
    var sum = 0
    var index = string.length - 1
    var result = 0
    do {
      if (string.charAt(index) == ')') {
        sum += 1
        if (sum == 0) result = index
      }
      if (string.charAt(index) == '(') {
        sum -= 1
        if (sum == 0) result = index
      }
      index -= 1
    } while (sum != 0)
    result
  }

  private def ParseComb(string: String):Either[String,C] = string match {
    case "S" => Right(C(S))
    case "K" => Right(C(K))
    case "I" => Right(C(I))
    case other => Left(s"Combinator ${other} is not in basis")
  }

  def ParseExpr(string: String): Either[String, Expr] = {
    PutAppMarker(string) match {
      case brackets(body) => ParseExpr(body)
      case comb(t) => ParseComb(t)
      case comb_comb(left, right) =>
        for {
          expr1 <- ParseExpr(left)
          expr2 <- ParseExpr(right)
        }  yield App(expr1,expr2)
      case withMarker(left, right) =>
        for {
          expr1 <- ParseExpr(left)
          expr2 <- ParseExpr(right)
        }  yield App(expr1,expr2)
      case _ => Left("Incorrect input")
    }
  }
}
